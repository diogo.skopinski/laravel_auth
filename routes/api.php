<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);    
});

Route::group(['prefix' => 'v1', 'middleware' => 'jwt.verify'],function () {
    Route::apiResources([
        'tasklist'  =>  'TaskListController',
      ]);

    Route::post('completedTaskList', 'TaskListController@completedTaskList')
        ->name('tasklist.completedTaskList');
    
    Route::post('logout', 'UserController@logout')->name('users.logout');
});

Route::get('index', 'TaskListController@index')->name('tasklist.index');
Route::get('show', 'TaskListController@show')->name('tasklist.show');
Route::post('store', 'TaskListController@store')->name('tasklist.store');
Route::put('update', 'TaskListController@update')->name('tasklist.update');
Route::delete('destroy', 'TaskListController@destroy')->name('tasklist.destroy');

Route::apiResources([
    'tasklist' => 'TaskListController',
    'tasks' => 'TasksController',
  ]);

Route::put('task/close/{id}', 'TasksController@closeTask')->name('tasks.closeTask');
Route::get('list/tasks/{id}', 'TasksController@tasksByList')->name('tasks.tasksByList');

